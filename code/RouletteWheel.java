import java.util.Random;

public class RouletteWheel {
    private Random rand;
    private int lastSpinNum;


    //constructor
    public RouletteWheel(){
        this.rand = new Random();
        this.lastSpinNum = 0;
    }

    //get method
    public int getValue(){
        return this.lastSpinNum;
    }

    //instance methods
    public void spin(){
    this.lastSpinNum = rand.nextInt(38);
    }
}