import java.util.Scanner;
public class Roulette{
    public static void main(String[] args){
        //creating RouletteWheel object
        RouletteWheel wheelObj = new RouletteWheel();

        //creating Scanner object
        Scanner scan =  new Scanner(System.in);

        double userMoney = 1000;
        System.out.println("Welcome to the fall 2023 roulette game!");
        System.out.println("As the player, you start with 1000$ ready to play. Would you like to place a bet? Enter 'Yes' or 'No'");
        String input = scan.next();

        if(input.equals("Yes")){
            System.out.println("Between 0-37, which number would you like to bet on?");
            int numBet = scan.nextInt();
            
            System.out.println("From 0-1000, how much money would you like to bet on?");
            double moneyBet = scan.nextDouble();
            wheelObj.spin();
            int wheelNum = wheelObj.getValue();
            
            System.out.println("...the number is..." + wheelNum);
            boolean betCheck = isBetRight(numBet, wheelNum);
            userMoney = newUserMoney(betCheck, userMoney, moneyBet);
            System.out.println("Your new amount of money is now..." + userMoney + "$");
        }
        else{
            System.out.println("Please come again to play!");
        }
    }

    //helper methods
    public static boolean isBetRight(int numBet, int wheelNum){
        boolean correctNum = false;
        if(numBet == wheelNum){
            System.out.println("Congratulations, you won, you guessed the right number!");
            correctNum = true;
        }
        else{
            System.out.println("Oh no! You lost, you guessed the wrong number");
        }
         return correctNum;
    }

    //calculcates the player's new amount of money after betting
    public static double newUserMoney (boolean bet, double userMoney, double moneyBet){
        double newMoney = 0;
        if(bet){
            newMoney = newMoney + (userMoney * moneyBet);
        }
        else{
            newMoney = userMoney - moneyBet;
        }
        return newMoney;
    }
}
